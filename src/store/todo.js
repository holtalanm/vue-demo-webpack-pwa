import ulid from 'ulid'
import db, { TodoStoreName } from './idb_setup'

const TodoBaseMutations = {
    setList: 'setList',
    add: 'add',
    delete: 'delete',
    update: 'update'
}

const TodoBaseActions = {
    add: TodoBaseMutations.add,
    delete: TodoBaseMutations.delete,
    getDBState: 'getDBState',
    update: TodoBaseMutations.update
}

export const TodoMutations = {
    setList: `${TodoStoreName}/${TodoBaseMutations.setList}`,
    add: `${TodoStoreName}/${TodoBaseMutations.add}`,
    delete: `${TodoStoreName}/${TodoBaseMutations.delete}`,
    update: `${TodoStoreName}/${TodoBaseMutations.update}`
}

export const TodoActions = {
    add: `${TodoStoreName}/${TodoBaseActions.add}`,
    delete: `${TodoStoreName}/${TodoBaseActions.delete}`,
    getDBState: `${TodoStoreName}/${TodoBaseActions.getDBState}`,
    update: `${TodoStoreName}/${TodoBaseActions.update}`
}

export class Todo {
    constructor (message, id, isDone) {
        this._id = (typeof id !== 'undefined' ? id : ulid())
        this._message = (typeof message !== 'undefined' ? message : '')
        this._isDone = (typeof isDone !== 'undefined' ? isDone : false)
    }

    get id () {
        return this._id
    }

    set id (id) {
        this._id = id
    }

    get message () {
        return this._message
    }

    set message (message) {
        this._message = message
    }

    get isDone () {
        return this._isDone
    }

    set isDone (isDone) {
        this._isDone = isDone
    }

    toJSON () {
        return {
            id: this.id,
            message: this.message,
            isDone: this.isDone
        }
    }

    static fromJSON (obj) {
        return new Todo(obj.message, obj.id, obj.isDone)
    }
}

export default {
    namespaced: true,
    state: {
        todos: []
    },
    // Synchronous
    mutations: {
        [TodoBaseMutations.setList] (state, list) {
            state.todos = list
        },

        [TodoBaseMutations.add] (state, todo) {
            state.todos.push(todo)
        },

        [TodoBaseMutations.delete] (state, todo) {
            state.todos = state.todos.filter((t) => { return t.id !== todo.id })
        },

        [TodoBaseMutations.update] (state, todo) {
            state.todos = state.todos.map(t => {
                if (t.id !== todo.id) {
                    return t
                } else {
                    t.isDone = todo.isDone
                    t.message = todo.message
                    return t
                }
            })
        }
    },
    // Asynchronous
    actions: {
        async [TodoBaseActions.getDBState] (context, todo) {
            let d = await db
            let ts = (await d.transaction(TodoStoreName).objectStore(TodoStoreName).getAll()).map(Todo.fromJSON)
            context.commit(TodoBaseMutations.setList, ts)
        },

        async [TodoBaseActions.add] (context, todo) {
            let d = await db
            let tx = d.transaction(TodoStoreName, 'readwrite')
            tx.objectStore(TodoStoreName).put(todo.toJSON())
            await tx.complete
            context.commit(TodoBaseMutations.add, todo)
        },

        async [TodoBaseActions.delete] (context, todo) {
            let d = await db
            let tx = d.transaction(TodoStoreName, 'readwrite')
            tx.objectStore(TodoStoreName).delete(todo.id)
            await tx.complete
            context.commit(TodoBaseMutations.delete, todo)
        },

        async [TodoBaseActions.update] (context, todo) {
            let d = await db
            let tx = d.transaction(TodoStoreName, 'readwrite')
            tx.objectStore(TodoStoreName).put(todo.toJSON())
            await tx.complete
            context.commit(TodoBaseMutations.update, todo)
        }
    }
}
